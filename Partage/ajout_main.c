#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "main_code_obstacles.h"
#include <code_obstacles.h>
#include <audio/play_melody.h>
#include <selector.h>
#include <leds.h>

#define VIES_DEPART 6

uint8_t vies;
int previousSelector = 0;
uint8_t boolJeu = 0;

static const uint16_t loseOneLife_melody[] = {
  NOTE_G2, NOTE_G2, NOTE_C2,
};

static const float loseOneLife_tempo[] = {
  9, 9, 9,
};

static const melody_t melodies[1] = {
	{
		.notes = loseOneLife_melody,
		.tempo = loseOneLife_tempo,
		.length = sizeof(loseOneLife_melody)/sizeof(uint16_t),
	},
};

void AllumerLeds(void){
	uint8_t yes = 1;

	palWritePad(GPIOD, GPIOD_LED1, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED3, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED5, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED7, yes ? 0 : 1);
}

void EteindreLeds(void){
	clear_leds();
}

int main (void){
	halInit();
	chSysInit();
	serial_start();


	ObstacleInit();
	playMelodyStart();

	vies = VIES_DEPART;

	playMelody(MARIO_DEATH,ML_SIMPLE_PLAY);
	chThdSleepMilliseconds(1000);
	ResetObstacle();
	AllumerLeds();
	chThdSleepMilliseconds(5000);
	EteindreLeds();

	while(1){
		if(TestFrequence() && boolJeu){
			LancerAutonome();
			boolJeu=0;
		}
		if(boolJeu){
			if(GetObstacle() && vies > 0){
				vies--;
				ResetObstacle();
				if(vies <= 0){
					stopCurrentMelody();
					waitMelodyHasFinished();
					playMelody(MARIO_DEATH,ML_SIMPLE_PLAY,NULL);
				} else {
					stopCurrentMelody();
					waitMelodyHasFinished();
					playMelody(EXTERNAL_SONG,ML_SIMPLE_PLAY,melodies[0]);
				}
			}
		}
	}
}

//À rajouter dans la boucle à la prochaine étape !!

void LancerJeu(void){
	playMelody(MARIO_DEATH,ML_SIMPLE_PLAY);
	chThdSleepMilliseconds(1000);
	ResetObstacle();
	AllumerLeds();
	chThdSleepMilliseconds(5000);
	EteindreLeds();
	boolJeu = 1;
	previousSelector = get_selector();
}

if(get_selector() != previousSelector){
	LancerJeu();
}



