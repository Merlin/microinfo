#include "ch.h"
#include "hal.h"
#include <main.h>
#include <usbcfg.h>
#include <chprintf.h>

#include <audio/microphone.h>
#include <audio.h>
#include <communications.h>
#include <fft.h>
#include <arm_math.h>
#include "mouvement.h"



//semaphore
static BSEMAPHORE_DECL(sendToComputer_sem, TRUE);

//2 times FFT_SIZE because these arrays contain complex numbers (real + imaginary)
static float micLeft_cmplx_input[2 * FFT_SIZE];
static float micRight_cmplx_input[2 * FFT_SIZE];
static float micFront_cmplx_input[2 * FFT_SIZE];
static float micBack_cmplx_input[2 * FFT_SIZE];
//Arrays containing the computed magnitude of the complex numbers
static float micLeft_output[FFT_SIZE];
static float micRight_output[FFT_SIZE];
static float micFront_output[FFT_SIZE];
static float micBack_output[FFT_SIZE];

#define SEUIL_SON	10000

#define MIN_FREQ			10	//we don't analyze before this index to not use resources for nothing
#define FREQ_FORWARD		16	//250Hz
#define FREQ_LEFT		19	//296Hz
#define FREQ_RIGHT		23	//359HZ
#define FREQ_BACKWARD	26	//406Hz
#define FREQ_RECHERCHE	29	//450Hz
#define FREQ_STOP		32	//498Hz
#define MAX_FREQ		34	//we don't analyze after this index to not use resources for nothing

#define FREQ_FORWARD_L		(FREQ_FORWARD-1)
#define FREQ_FORWARD_H		(FREQ_FORWARD+1)
#define FREQ_LEFT_L			(FREQ_LEFT-1)
#define FREQ_LEFT_H			(FREQ_LEFT+1)
#define FREQ_RIGHT_L		(FREQ_RIGHT-1)
#define FREQ_RIGHT_H		(FREQ_RIGHT+1)
#define FREQ_BACKWARD_L		(FREQ_BACKWARD-1)
#define FREQ_BACKWARD_H		(FREQ_BACKWARD+1)
#define FREQ_RECHERCHE_L		(FREQ_RECHERCHE-1)
#define FREQ_RECHERCHE_H		(FREQ_RECHERCHE+1)
#define FREQ_STOP_L			(FREQ_STOP-1)
#define FREQ_STOP_H			(FREQ_STOP+1)

#define HIST_SIZE			20

static uint8_t frequence = 0;
uint8_t blocked = 0;

void AnalyseFrequence(float *micData){
	float max = SEUIL_SON;
	int8_t index = -1;

	for(uint8_t i = MIN_FREQ; i<=MAX_FREQ; i++){
		if(micData[i] > max){
			index = i;
			max = micData[i];
		}
	}

	if(index >= FREQ_FORWARD_L && index <= FREQ_FORWARD_H){
		frequence = 1;
	}
	if(index >= FREQ_LEFT_L && index <= FREQ_LEFT_H){
		frequence = 2;
	}
	if(index >= FREQ_RIGHT_L && index <= FREQ_RIGHT_H){
		frequence = 3;
	}
	if(index >= FREQ_BACKWARD_L && index <= FREQ_BACKWARD_H){
		frequence = 4;
	}
	if(index >= FREQ_RECHERCHE_L && index <= FREQ_RECHERCHE_H){
		frequence = 5;
	}
	if(index >= FREQ_STOP_L && index <= FREQ_STOP_H){
		frequence = 6;
	}
}

void CommandeDeplacement(void){
	switch(frequence){
		case 1:
			AllerToutDroit();
			break;
		case 2:
			TournerGauche();
			break;
		case 3:
			TournerDroite();
			break;
		case 4:
			AllerEnArriere();
			break;
		case 6:
			StopperMoteurs();
			break;
		default:
			break;
	}
}

void RechercheDirection(float *dataLeft,float *dataRight,float *dataFront,float *dataBack,
				float *dataCmplxLeft, float *dataCmplxRight,float *dataCmplxFront, float *dataCmplxBack){

	//Déclaration des variables
	float max_left = SEUIL_SON;
	float max_right = SEUIL_SON;
	float max_front = SEUIL_SON;
	int8_t index_left = -1;
	int8_t index_right = -1;
	int8_t index_front = -1;
	float arg_left = 0;
	float arg_right = 0;
	float arg_front = 0;
	float arg_diffH = 0;
	float arg_diffV = 0;

	//Trouver les maximums d'amplitudes
	for(uint8_t i = MIN_FREQ ; i <= MAX_FREQ ; i++){
		if(dataLeft[i] > max_left){
			max_left = dataLeft[i];
			index_left = i;
		}
	}
	for(uint8_t i = MIN_FREQ ; i <= MAX_FREQ ; i++){
		if(dataRight[i] > max_right){
			max_right = dataRight[i];
			index_right = i;
		}
	}
	for(uint8_t i = MIN_FREQ ; i <= MAX_FREQ ; i++){
		if(dataFront[i] > max_front){
			max_front = dataFront[i];
			index_front = i;
		}
	}

	//Calcul des arguments complexes (phases)
	arg_left =atan2f(dataCmplxLeft[(2*index_left)+1], dataCmplxLeft[(2*index_left)]);
	arg_right =atan2f(dataCmplxRight[(2*index_right)+1], dataCmplxRight[(2*index_right)]);
	arg_front =atan2f(dataCmplxFront[(2*index_front)+1], dataCmplxRight[(2*index_front)]);
	arg_diffH = arg_left-arg_right;
	arg_diffV = arg_front - ((arg_left+arg_right)/2);
	arg_diffH= 360*(arg_diffH)/TWO_PI; //Conversion en degrés
	arg_diffV= 360*(arg_diffV)/TWO_PI; //Conversion en degrés

	/*
	//Mise en valeur absolue (peut-être inutile)
	if(arg_diffH < 0){
		arg_diffH = -arg_diffH;
	}
	*/

	//Mouvements d'alignement (à modifier avec les essais)
	if(index_left == index_right == index_front){
		if(arg_diffH < -5){
			TournerDroite();
		}else if(arg_diffH > 5){
			TournerGauche();
		}else if(arg_diffV > 0){
			AllerToutDroit();
		}
	}
}

/*
*	Callback called when the demodulation of the four microphones is done.
*	We get 160 samples per mic every 10ms (16kHz)
*	
*	params :
*	int16_t *data			Buffer containing 4 times 160 samples. the samples are sorted by micro
*							so we have [micRight1, micLeft1, micBack1, micFront1, micRight2, etc...]
*	uint16_t num_samples	Tells how many data we get in total (should always be 640)
*/

void ProcessAudio(int16_t *data, uint16_t num_samples){

	/*
	*
	*	We get 160 samples per mic every 10ms
	*	So we fill the samples buffers to reach
	*	1024 samples, then we compute the FFTs.
	*
	*/

	static uint16_t nb_samples = 0;
	static uint8_t mustSend = 0;

	//loop to fill the buffers
	for(uint16_t i = 0 ; i < num_samples ; i+=4){
		//construct an array of complex numbers. Put 0 to the imaginary part
		micRight_cmplx_input[nb_samples] = (float)data[i + MIC_RIGHT];
		micLeft_cmplx_input[nb_samples] = (float)data[i + MIC_LEFT];
		micBack_cmplx_input[nb_samples] = (float)data[i + MIC_BACK];
		micFront_cmplx_input[nb_samples] = (float)data[i + MIC_FRONT];

		nb_samples++;

		micRight_cmplx_input[nb_samples] = 0;
		micLeft_cmplx_input[nb_samples] = 0;
		micBack_cmplx_input[nb_samples] = 0;
		micFront_cmplx_input[nb_samples] = 0;

		nb_samples++;

		//stop when buffer is full
		if(nb_samples >= (2 * FFT_SIZE)){
			break;
		}
	}

	if(nb_samples >= (2 * FFT_SIZE)){
		/*	FFT proccessing
		*
		*	This FFT function stores the results in the input buffer given.
		*	This is an "In Place" function. 
		*/

		doFFT_optimized(FFT_SIZE, micRight_cmplx_input);
		doFFT_optimized(FFT_SIZE, micLeft_cmplx_input);
		doFFT_optimized(FFT_SIZE, micFront_cmplx_input);
		doFFT_optimized(FFT_SIZE, micBack_cmplx_input);

		/*	Magnitude processing
		*
		*	Computes the magnitude of the complex numbers and
		*	stores them in a buffer of FFT_SIZE because it only contains
		*	real numbers.
		*
		*/
		arm_cmplx_mag_f32(micRight_cmplx_input, micRight_output, FFT_SIZE);
		arm_cmplx_mag_f32(micLeft_cmplx_input, micLeft_output, FFT_SIZE);
		arm_cmplx_mag_f32(micFront_cmplx_input, micFront_output, FFT_SIZE);
		arm_cmplx_mag_f32(micBack_cmplx_input, micBack_output, FFT_SIZE);

		//sends only one FFT result over 10 for 1 mic to not flood the computer
		//sends to UART3

		if(mustSend > 8){
			//signals to send the result to the computer
			chBSemSignal(&sendToComputer_sem);
			mustSend = 0;
		}

		nb_samples = 0;
		mustSend++;

		AnalyseFrequence();
		if(!blocked){
			if(frequence == 5){
				RechercheDirection(micLeft_output,micRight_output,micFront_output,micBack_output,
						micLeft_cmplx_input,micRight_cmplx_input,micFront_cmplx_input,micBack_cmplx_input);
			} else {
				CommandeDeplacement();
			}
		}
	}
}

void BloquerAudio(void){
	blocked = 1;
}

void DebloquerAudio(void){
	blocked = 0;
}

uint8_t TestFrequence(void){
	if(frequence == 5){
		return 1;
	}
	return 0;
}
