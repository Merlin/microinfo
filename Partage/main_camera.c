float distance = get_distance();

if(distance > 9.0f && distance < 11.0f){
	uint8_t yes = 1;

	palWritePad(GPIOD, GPIOD_LED1, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED3, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED5, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED7, yes ? 0 : 1);
}

