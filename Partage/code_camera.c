#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ch.h"
#include "hal.h"
#include <usbcfg.h>

#include <camera/po8030.h>
#include <camera/dcmi_camera.h>

#include "code_camera.h"

#define IMAGE_BUFFER_SIZE 		640
#define WIDTH_SLOPE				5
#define MIN_LINE_WIDTH			40
#define PXTOCM					1570.0f //experimental value
#define GOAL_DISTANCE 			10.0f
#define MAX_DISTANCE 			25.0f

static float distance = 0;

/*
 *  Returns the line's width extracted from the image buffer given
 *  Returns 0 if line not found
 */
uint16_t extract_line_width(uint8_t *buffer){

	uint16_t i = 0, begin = 0, end = 0, width = 0;
	uint8_t stop = 0, wrong_line = 0, line_not_found = 0;
	uint32_t mean = 0;

	static uint16_t last_width = PXTOCM/GOAL_DISTANCE;

	//performs an average
	for(uint16_t i = 0 ; i < IMAGE_BUFFER_SIZE ; i++){
		mean += buffer[i];
	}
	mean /= IMAGE_BUFFER_SIZE;

	do{
		wrong_line = 0;
		//search for a begin
		while(stop == 0 && i < (IMAGE_BUFFER_SIZE - WIDTH_SLOPE))
		{
			//the slope must at least be WIDTH_SLOPE wide and is compared
		    //to the mean of the image
		    if(buffer[i] > mean && buffer[i+WIDTH_SLOPE] < mean)
		    {
		        begin = i;
		        stop = 1;
		    }
		    i++;
		}
		//if a begin was found, search for an end
		if (i < (IMAGE_BUFFER_SIZE - WIDTH_SLOPE) && begin)
		{
		    stop = 0;

		    while(stop == 0 && i < IMAGE_BUFFER_SIZE)
		    {
		        if(buffer[i] > mean && buffer[i-WIDTH_SLOPE] < mean)
		        {
		            end = i;
		            stop = 1;
		        }
		        i++;
		    }
		    //if an end was not found
		    if (i > IMAGE_BUFFER_SIZE || !end)
		    {
		        line_not_found = 1;
		    }
		}
		else//if no begin was found
		{
		    line_not_found = 1;
		}

		//if a line too small has been detected, continues the search
		if(!line_not_found && (end-begin) < MIN_LINE_WIDTH){
			i = end;
			begin = 0;
			end = 0;
			stop = 0;
			wrong_line = 1;
		}
	}while(wrong_line);

	if(line_not_found){
		begin = 0;
		end = 0;
		return 0;
	}else{
		last_width = width = (end - begin);
		//line_position = (begin + end)/2; //gives the line position.
	}

	//sets a maximum width or returns the measured width
	if((PXTOCM/width) > MAX_DISTANCE){
		return PXTOCM/MAX_DISTANCE;
	}else{
		return width;
	}
}

//déclaration de la sémaphore
static BSEMAPHORE_DECL(sem_image_prete, TRUE);

static THD_WORKING_AREA(waCaptureCameraThd, 128);
static THD_FUNCTION(CaptureCameraThd, arg) {

	chRegSetThreadName(__FUNCTION__);
	(void)arg;

	//Préparation de la capture (ATTENTION, uniquement lignes 10 et 11)
	po8030_advanced_config(FORMAT_RGB565, 0, 10, IMAGE_BUFFER_SIZE, 2, SUBSAMPLING_X1, SUBSAMPLING_X1);
	dcmi_enable_double_buffering();
	dcmi_set_capture_mode(CAPTURE_ONE_SHOT);
	dcmi_prepare();

	while(1){
		//Lancer la capture de la caméra
		dcmi_capture_start();
		//Attendre que la capture soit terminée
		wait_image_ready();
		//Lancer le signal à l'autre thread que la capture est terminée
		chBSemSignal(&sem_image_prete);
	}
}

static THD_WORKING_AREA(waProcessCameraThd, 128);
static THD_FUNCTION(ProcessCameraThd, arg) {

	chRegSetThreadName(__FUNCTION__);
	(void)arg;

	uint8_t *pointeurImage;
	uint8_t image[IMAGE_BUFFER_SIZE] = {0};
	uint16_t largeur;

	while(1){
		// Attend le signal de la capture terminée
		chBSemWait(&sem_image_prete);

		pointeurImage = dcmi_get_last_image_ptr();

		//Extraire seulement le rouge (donc 5 bits du premier byte et aucun du second)
		for(uint16_t i = 0 ; i < (2 * IMAGE_BUFFER_SIZE) ; i+=2){
			image[i/2] = (uint8_t)img_buff_ptr[i]&0xF8;
		}

		largeur = extract_line_width(image);

		//convertir la largeur en distance
		if(largeur){
			distance = PXTOCM/largeur;
		}
	}
}

float get_distance(void){
	return distance;
}
