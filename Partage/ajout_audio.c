#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "main_code_obstacles.h"
#include <code_obstacles.h>
#include <audio/play_melody.h>
#include <selector.h>
#include <leds.h>

#define SEUIL_COMPTEUR 5

uint8_t boolRecherche = 0;

void AnalyseFrequence(float *micData){
	float max = SEUIL_SON;
	static uint8_t compteur;
	static uint8_t previousFrequence;
	uint8_t newFrequence = 0;
	int8_t index = -1;

	for(uint8_t i = MIN_FREQ; i<=MAX_FREQ; i++){
		if(micData[i] > max){
			index = i;
			max = micData[i];
		}
	}

	if(index >= FREQ_FORWARD_L && index <= FREQ_FORWARD_H){
		newFrequence = 1;
	}
	if(index >= FREQ_LEFT_L && index <= FREQ_LEFT_H){
		newFrequence = 2;
	}
	if(index >= FREQ_RIGHT_L && index <= FREQ_RIGHT_H){
		newFrequence = 3;
	}
	if(index >= FREQ_BACKWARD_L && index <= FREQ_BACKWARD_H){
		newFrequence = 4;
	}

	if(index >= FREQ_RECHERCHE_L && index <= FREQ_RECHERCHE_H){
		newFrequence = 5;
	}

	if(index >= FREQ_STOP_L && index <= FREQ_STOP_H){
		newFrequence = 6;
	}

	if(newFrequence == previousFrequence){
		compteur ++;
		if(compteur > SEUIL_COMPTEUR){
			frequence = newFrequence;
			if(frequence == 5){
				boolRecherche = 1;
			}
		}
	} else {
		compteur = 0;
	}

	previousFrequence = newFrequence;
}

uint8_t TestRecherche(void){
	return boolRecherche;
}

void StopperRecherche(void){
	boolRecherche = 0;
}

//À mettre dans processAudio
AnalyseFrequence();
if(!blocked){
	if(boolRecherche){
		if(frequence == 5){
			RechercheDirection(micLeft_output,micRight_output,micFront_output,micBack_output,
				micLeft_cmplx_input,micRight_cmplx_input,micFront_cmplx_input,micBack_cmplx_input);
		}
	} else {
		CommandeDeplacement();
	}
}
