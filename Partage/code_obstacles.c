#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "code_obstacles.h"
#include "main_code_obstacles.h"

#define SEUIL_COMPTEUR_OBSTACLE 5
#define SEUIL_OBSTACLE 50
#define VIES_DEPART 3

uint8_t vies;

uint8_t PlusProcheObstacle(void){
	int maxValue = SEUIL_OBSTACLE;
	uint8_t index = 8;
	for(uint8_t i = 0; i < 8; i++){
		int value = get_prox(i);
		if(value<0){
			value= -value;
		}
		if(value > maxValue){
			maxValue = value ;
			index = i;
		}
	}
	return index;
}

uint8_t TestObstacle(void){
	static uint8_t previousIndex = 9;
	static uint8_t compteur = 0;

	uint8_t index;
	index = PlusProcheObstacle();
	if(index == previousIndex){
		compteur ++;
		if(compteur > SEUIL_COMPTEUR_OBSTACLE){
			return 1;
		}
	}else {
		compteur = 0;
	}
	previousIndex = index;
	return 0;
}

void ObstacleInit(){
	vies = VIES_DEPART;
}

static THD_WORKING_AREA(waCodeObstacleThd, 128);
static THD_FUNCTION(CodeObstacleThd, arg) {

	chRegSetThreadName(__FUNCTION__);
	(void)arg;

	while(1){
		switch(etatPuck){
		case EN_FORME:
			if(TestObstacle()){
					vies --;
					if(vies <= 0){
						etatPuck = MORT;
					}
				}
			break;
		case AUTOMATIQUE:
			//Déplacement tout droit selon les obstacles
			break;
		case MORT:
			break;
		}
	}
}
