#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ch.h"
#include "hal.h"
#include "memory_protection.h"
#include <motors.h>
#include <main.h>
#include <sensors/proximity.h>

messagebus_t bus;
MUTEX_DECL(bus_lock);
CONDVAR_DECL(bus_condvar);

//Je sais pas si on a besoin de cette fonction
static void serial_start(void)
{
	static SerialConfig ser_cfg = {
	    115200,
	    0,
	    0,
	    0,
	};

	sdStart(&SD3, &ser_cfg); // UART3.
}

void show_proximity(proximity_msg_t *prox_values){

	unsigned int *ambientValues = prox_values->ambient;
	unsigned int *reflectedValues = prox_values->reflected;
	unsigned int *deltaValues;
	unsigned int maxValue = 0;
	unsigned int valueSeuil;

	uint8_t index;

	//Commenter la ligne 44 si on veut décommenter cette ligne
	//deltaValues = prox_values -> delta;

	for(uint8_t i = 0; i < 8; i++){

		//décommenter la ligne 39 si on veut commenter cette ligne
		deltaValues[i] = reflectedValues[i] - ambientValues[i];

		if(deltaValues[i] > maxValue){
			maxValue = deltaValues[i];
			index = i;
		}
	}

	uint8_t frontLed = 0, rightLed = 0, backLed = 0, leftLed = 0;
	if(maxValue > valueSeuil){

		switch(index)
			{
				case 0 :
					frontLed = 1;
					break;
				case 1 :
					frontLed = 1;
					rightLed = 1;
					break;
				case 2 :
					rightLed = 1;
					break;
				case 3 :
					backLed = 1;
					break;
				case 4 :
					backLed = 1;
					break;
				case 5 :
					leftLed = 1;
					break;
				case 6 :
					leftLed = 1;
					frontLed = 1;
					break;
				case 7 :
					frontLed = 1;
					break;
			}

		//commande les moteurs
		if(frontLed == 1){
			left_motor_set_speed(-600);
			right_motor_set_speed(-600);
		}

		if(rightLed == 1){
			left_motor_set_speed(-600);
			right_motor_set_speed(600);
		}

		if(leftLed == 1){
			left_motor_set_speed(600);
			right_motor_set_speed(-600);
		}

		if(backLed == 1){
			left_motor_set_speed(600);
			right_motor_set_speed(600);
		}
	}

	//Change les leds
	palWritePad(GPIOD, GPIOD_LED1, frontLed ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED3, rightLed ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED5, backLed ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED7, leftLed ? 0 : 1);
}

int main(void)
{

    halInit();
    chSysInit();

    //starts the serial communication (pas sûr qu'on en ait besoin
    serial_start();

    motors_init();

    messagebus_init(&bus, &bus_lock, &bus_condvar);

    proximity_start();

    messagebus_topic_t *proximity_topic = messagebus_find_topic_blocking(&bus, "/proximity");
    proximity_msg_t prox_values;


    /* Infinite loop. */
    while (1) {

        messagebus_topic_wait(proximity_topic, &prox_values, sizeof(prox_values));

        show_proximity(&prox_values);
    	//waits 1 second
        chThdSleepMilliseconds(1000);
    }
}

//Je sais pas trop à quoi ça sert
#define STACK_CHK_GUARD 0xe2dee396
uintptr_t __stack_chk_guard = STACK_CHK_GUARD;

void __stack_chk_fail(void)
{
    chSysHalt("Stack smashing detected");
}
