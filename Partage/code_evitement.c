#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define VITESSE 600
#define SEUIL_OBSTACLE 50
#define STEPS_GRAND_VIRAGE 162
#define STEPS_PETIT_VIRAGE 81
#define STEPS_TOUT_DROIT 800

uint8_t gauche = 0;
uint8_t court = 0;
uint8_t etat = 0;

void TournerGauche (void){
	left_motor_set_speed(-VITESSE);
	right_motor_set_speed(VITESSE);
	left_motor_set_pos(0);
	right_motor_set_pos(0);
}

void TournerDroite(void){
	left_motor_set_speed(VITESSE);
	right_motor_set_speed(-VITESSE);
	left_motor_set_pos(0);
	right_motor_set_pos(0);
}

uint8_t TestFinVirage(){
	int32_t compteur;
	compteur = left_motor_get_pos();
	if(compteur<0){
		compteur = -compteur;
	}
	if(court){
		if(compteur >= STEPS_PETIT_VIRAGE){
			return 1;
		}
	}else{
		if(compteur >= STEPS_VIRAGE_DROIT){
			return 1;
		}
	}
	return 0;
}

uint8_t TestFinToutDroit(void){
	int32_t compteur;
	compteur = left_motor_get_pos();
	if(compteur < 0){
		compteur = -compteur;
	}
	if(compteur >= STEPS_TOUT_DROIT){
		return 1;
	}
	return 0;
}

void AllerToutDroit(void){
	left_motor_set_speed(VITESSE);
	right_motor_set_speed(VITESSE);
	left_motor_set_pos(0);
	right_motor_set_pos(0);
}

uint8_t PlusProcheObstacle(void){
	int maxValue = 0;
	uint8_t index;
	for(uint8_t i; i < 8; i++){
		int value = get_prox(i);
		if(value<0){
			value= -value;
		}
		if(value > maxValue){
			maxValue = value ;
			index = i;
		}
	}
	return index;
}

uint8_t TestObstacle(void){
	for(uint8_t i; i < 8; i++){
		int value = get_prox(i);
		if(value < 0){
			value = -value;
		}
		if(value > SEUIL_OBSTACLE){
			return 1;
		}
	}
}

void DeplacementAutonome(void){
	switch(etat){
	case 0 :
		if(TestObstacle()){
			uint8_t index = PlusProcheObstacle();
			switch(index){
				case 0 :
					TournerGauche();
					gauche = 0;
					court = 0;
					etat = 1;
					break;
				case 1 :
					TournerGauche();
					gauche = 0;
					court = 1;
					etat = 1;
					break;
				case 6 :
					TournerDroite();
					gauche = 1;
					court = 1;
					etat = 1;
					break;
				case 7 :
					TournerDroite();
					gauche = 1;
					court = 0;
					etat = 1;
					break;
				default :
					break;
			}
		}
		break;
	case 1 :
		if(TestFinVirage()){
			AllerToutDroit();
			etat = 2;
		}
		break;
	}
	case 2:
		if(TestFinToutDroit()){
			if(gauche){
				TournerGauche();
				etat = 3;
			}else{
				TournerDroite();
				etat = 3;
			}
		}
		break;
	case 3:
		if(TestFinVirage()){
			AllerToutDroit();
			etat = 0;
		}
		break;
	}
}
