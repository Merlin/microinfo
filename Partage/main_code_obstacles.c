#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "main_code_obstacles.h"
#include <code_obstacles.h>
#include <audio/play_melody.h>
#include <selector.h>
#include <leds.h>

void AllumerLeds(void){
	uint8_t yes = 1;

	palWritePad(GPIOD, GPIOD_LED1, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED3, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED5, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED7, yes ? 0 : 1);
}

void AllumerLedUnique(int ledNumber){
	clear_leds();
	set_led((ledNumber%4),1);
}

int main (void){
	halInit();
	chSysInit();
	serial_start();

	etatPuck = EN_FORME;

	ObstacleInit();
	playMelodyStart();

	chThdCreateStatic(waCodeObstacleThd, sizeof(waCodeObstacleThd), NORMALPRIO, CodeObstacleThd, NULL);

	while(1){
		int selector = get_selector();
		AllumerLedUnique(selector);
		selector = selector %2;
		switch(selector){
			case JEU :
				if(etatPuck == MORT){
					AllumerLeds();
					playMelody(MARIO_DEATH,ML_SIMPLE_PLAY);
				} else {
					//Déplacement selon les FFT etc...
				}
				break;
			case AUTONOME :
				//Deplacement Autonome
				break;
		}
	}
}



