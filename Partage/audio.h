#ifndef CODE_OBSTACLE_H
#define CODE_OBSTACLE_H

#include <ch.h>
#include <hal.h>

void ProcessAudio(int16_t *data, uint16_t num_samples);

void BloquerAudio(void);
void DebloquerAudio(void);

#endif
