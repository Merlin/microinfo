#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ch.h"
#include "hal.h"
#include "memory_protection.h"
#include <motors.h>
#include <main.h>

#define VITESSE 				600
#define COUNT_TO_DEGRES 		0.5555
#define COUNT_TO_CM 			0.026
#define ANGLE_MAX 			360
#define DEGRES_TO_RADIANS	0.0174

float position;
float angle;

int8_t notVirage;

uint8_t blocked = 0;

uint8_t TestFinSteps(int32_t limiteSteps){
	int32_t compteur;
	compteur = left_motor_get_pos();
	if(compteur<0){
		compteur = -compteur;
	}
	if(compteur >= limiteSteps){
		return 1;
		ResetCompteur();
	}else{
		return 0;
	}
}

void AllerToutDroit(void){
	if(!blocked){
		if(notVirage){
			UpdatePosition();
		} else {
			UpdateAngle();
		}
		notVirage = 1;
		left_motor_set_speed(VITESSE);
		right_motor_set_speed(VITESSE);
	}
}

void AllerEnArriere(void){
	if(!blocked){
		UpdateAngle();
		notVirage = 1;
		left_motor_set_speed(-VITESSE);
		right_motor_set_speed(-VITESSE);
	}
}

void TournerGauche(void){
	if(!blocked){
		UpdatePosition();
		notVirage = 0;
		left_motor_set_speed(-VITESSE);
		right_motor_set_speed(VITESSE);
	}
}

void TournerDroite(void){
	if(!blocked){
		UpdatePosition();
		notVirage = 0;
		left_motor_set_speed(VITESSE);
		right_motor_set_speed(-VITESSE);
	}
}

void BloquerMoteurs(){
	StopperMoteurs();
	blocked = 1;
}

void DebloquerMoteurs(){
	blocked = 0;
}
