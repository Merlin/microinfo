#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ch.h"
#include "hal.h"
#include "memory_protection.h"
#include <motors.h>
#include <main.h>

#define VITESSE 				600
#define COUNT_TO_DEGRES 		0.5555
#define COUNT_TO_CM 			0.026
#define ANGLE_MAX 			360
#define DEGRES_TO_RADIANS	0.0174

float position;
float angle;

int8_t notVirage;

void ResetCompteur(void){
	left_motor_set_pos(0);
	right_motor_set_pos(0);
}

void InitMouvement(void){
	position = 0;
	angle = 0;
}

void UpdateAngle(void){
	int32_t compteur = right_motor_get_pos();
	angle += compteur * COUNT_TO_DEGRES;
	if(angle< 0){
		angle = ANGLE_MAX -angle;
	}
	if(angle > ANGLE_MAX){
		angle = angle - ANGLE_MAX;
	}
	ResetCompteur();
}

void UpdatePosition(void){
	int32_t compteur = right_motor_get_pos();
	float hyp = compteur * COUNT_TO_CM;
	float cosAngle = cosf(angle * DEGRES_TO_RADIANS);

	position += cosAngle*hyp;

	ResetCompteur();
}

void AllerToutDroit(void){
	if(!blocked){
		UpdateAngle();
		notVirage = 1;
		left_motor_set_speed(VITESSE);
		right_motor_set_speed(VITESSE);
	}
}

void AllerEnArriere(void){
	if(!blocked){
		UpdateAngle();
		notVirage = 1;
		left_motor_set_speed(-VITESSE);
		right_motor_set_speed(-VITESSE);
	}
}

void TournerGauche(void){
	if(!blocked){
		UpdatePosition();
		notVirage = 0;
		left_motor_set_speed(-VITESSE);
		right_motor_set_speed(VITESSE);
	}
}

void TournerDroite(void){
	if(!blocked){
		UpdatePosition();
		notVirage = 0;
		left_motor_set_speed(VITESSE);
		right_motor_set_speed(-VITESSE);
	}
}

void StopperMoteurs(void){
	left_motor_set_speed(0);
	right_motor_set_speed(0);
}

float GetPosition(void){
	if(notVirage){
		UpdatePosition();
	}
	return position;
}
