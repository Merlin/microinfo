/*
 * 	audio.c
 *	Projet de Microinformatique: Guitar_Hero_Epuck2
 *  Created on: 4 mai 2020
 *  Author: Merlin et Pierre
 */


#include <audio/microphone.h>
#include <audio.h>
#include <fft.h>
#include <arm_math.h>
#include "mouvement.h"
#include "ch.h"
#include "hal.h"
#include "main.h"
//Taille des tableaux de FFT
#define FFT_SIZE 	1024
#define HALF_FFT_SIZE 	512
//Valeur utilis�e pour les calculs complexes
#define TWO_PI 6.28

//semaphore (non utilis�)
static BSEMAPHORE_DECL(sendToComputer_sem, TRUE);

//2 times FFT_SIZE because these arrays contain complex numbers (real + imaginary)
static float micLeft_cmplx_input[2 * FFT_SIZE];
static float micRight_cmplx_input[2 * FFT_SIZE];
static float micFront_cmplx_input[2 * FFT_SIZE];
static float micBack_cmplx_input[2 * FFT_SIZE];
//Arrays containing the computed magnitude of the complex numbers
static float micLeft_output[FFT_SIZE];
static float micRight_output[FFT_SIZE];
static float micFront_output[FFT_SIZE];
static float micBack_output[FFT_SIZE];
//Seuil minimum pour prendre en compte un son
#define SEUIL_SON	10000
//fr�quences de controle
#define MIN_FREQ		7	//we don't analyze before this index to not use resources for nothing
#define FREQ_FORWARD	9	//140Hz
#define FREQ_LEFT		12	//186Hz
#define FREQ_RIGHT		15	//233HZ
#define FREQ_BACKWARD	20	//311Hz
#define FREQ_STOP		24	//372Hz
#define FREQ_RECHERCHE	29	//450Hz
#define MAX_FREQ		31	//we don't analyze after this index to not use resources for nothing
//Plages de d�tection
#define FREQ_FORWARD_L		(FREQ_FORWARD-1)
#define FREQ_FORWARD_H		(FREQ_FORWARD+1)
#define FREQ_LEFT_L			(FREQ_LEFT-1)
#define FREQ_LEFT_H			(FREQ_LEFT+1)
#define FREQ_RIGHT_L		(FREQ_RIGHT-1)
#define FREQ_RIGHT_H		(FREQ_RIGHT+1)
#define FREQ_BACKWARD_L		(FREQ_BACKWARD-1)
#define FREQ_BACKWARD_H		(FREQ_BACKWARD+1)
#define FREQ_RECHERCHE_L	(FREQ_RECHERCHE-1)
#define FREQ_RECHERCHE_H	(FREQ_RECHERCHE+1)
#define FREQ_STOP_L			(FREQ_STOP-1)
#define FREQ_STOP_H			(FREQ_STOP+1)

//Marges admissibles pour les diff�rences de phase

#define ARG_MAX_TOUT_DROIT  15
#define ARG_MAX_VIRAGE 		5

//Nombre de r�p�titions n�cessaires pour prendre en compte un son (filtrage)

#define SEUIL_COMPTEUR		5

//Variable qui stock la fr�quence ayant la plus grande amplitude

static uint8_t frequence = 0;

//Bool�ens qui d�terminent quelles fonctions de traitement vont �tre utilis�es

static uint8_t blockedAudio = FALSE;
static uint8_t boolRecherche = FALSE;

//Analyse quelle est la fr�quence qui � l'amplitude la plus haute, puis filtre si cette fr�quence est r�p�t�e plusieurs fois de suite.
//Au quel cas, on stock cette fr�quence dans la variable fr�quence.

void AnalyseFrequence(float *micData){
	float max = SEUIL_SON;
	static uint8_t compteur;
	static uint8_t previousFrequence;
	uint8_t newFrequence = 0;
	int8_t index = -1;
	//Trouve la fr�quence la plus haute.
	for(uint8_t i = MIN_FREQ; i<=MAX_FREQ; i++){
		if(micData[i] > max){
			index = i;
			max = micData[i];
		}
	}
	//D�termine � quelle commande correspond cette fr�quence.
	if(index >= FREQ_FORWARD_L && index <= FREQ_FORWARD_H){
		newFrequence = FREQ_FORWARD;
	}
	if(index >= FREQ_LEFT_L && index <= FREQ_LEFT_H){
		newFrequence = FREQ_LEFT;
	}
	if(index >= FREQ_RIGHT_L && index <= FREQ_RIGHT_H){
		newFrequence = FREQ_RIGHT;
	}
	if(index >= FREQ_BACKWARD_L && index <= FREQ_BACKWARD_H){
		newFrequence = FREQ_BACKWARD;
	}

	if(index >= FREQ_RECHERCHE_L && index <= FREQ_RECHERCHE_H){
		newFrequence = FREQ_RECHERCHE;
	}

	if(index >= FREQ_STOP_L && index <= FREQ_STOP_H){
		newFrequence = FREQ_STOP;
	}
	//Si c'est la m�me fr�quence plusieurs fois de suite, assignement � la variable fr�quence.
	if(newFrequence == previousFrequence){
		compteur ++;
		if(compteur > SEUIL_COMPTEUR){
			frequence = newFrequence;
			//Si la fr�quence est la fr�quence recherche, lancement du mode retour.
			if(frequence == FREQ_RECHERCHE){
				boolRecherche = TRUE;
			}
		}
	} else {
		compteur = 0;
	}

	previousFrequence = newFrequence;
}

//Lance les mouvements en fonction de la fr�quence s�lectionn�e
void CommandeDeplacement(void){
	switch(frequence){
		case FREQ_FORWARD:
			SetVitesse(VITESSE_MAX);
			AllerToutDroit();
			break;
		case FREQ_LEFT:
			SetVitesse(VITESSE_VIRAGE);
			TournerGauche();
			break;
		case FREQ_RIGHT:
			SetVitesse(VITESSE_VIRAGE);
			TournerDroite();
			break;
		case FREQ_BACKWARD:
			SetVitesse(VITESSE_MAX);
			AllerEnArriere();
			break;
		case FREQ_STOP:
			StopperMoteurs();
			break;
		default:
			break;
	}
}
//Calcul de la direction du son avec les micros gauche, droit, et avant.
//Le sens du virage est d�termin� par la diff�rence de phase, et d�place les moteurs de sorte que l'Epuck aille en direction du son.
void RechercheDirection(float *dataLeft,float *dataRight,float *dataFront,
				float *dataCmplxLeft, float *dataCmplxRight,float *dataCmplxFront){

	//D�claration des variables
	float max_left = SEUIL_SON;
	float max_right = SEUIL_SON;
	float max_front = SEUIL_SON;
	int8_t index_left = -1;
	int8_t index_right = -1;
	int8_t index_front = -1;
	float arg_left = 0;
	float arg_right = 0;
	float arg_front = 0;
	float arg_diffH = 0;
	float arg_diffV = 0;
	//Bool�en qui d�crit si l'Epuck est align� avec le son ou non
	static uint8_t align =FALSE;

	//Trouver les maximums d'amplitudes de chaque micro
	for(uint8_t i = MIN_FREQ ; i <= MAX_FREQ ; i++){
		if(dataLeft[i] > max_left){
			max_left = dataLeft[i];
			index_left = i;
		}
	}
	for(uint8_t i = MIN_FREQ ; i <= MAX_FREQ ; i++){
		if(dataRight[i] > max_right){
			max_right = dataRight[i];
			index_right = i;
		}
	}
	for(uint8_t i = MIN_FREQ ; i <= MAX_FREQ ; i++){
		if(dataFront[i] > max_front){
			max_front = dataFront[i];
			index_front = i;
		}
	}


	//Calcul des arguments complexes (phases)
	arg_left =atan2f(dataCmplxLeft[(2*index_left)+1], dataCmplxLeft[(2*index_left)]);
	arg_right =atan2f(dataCmplxRight[(2*index_right)+1], dataCmplxRight[(2*index_right)]);
	arg_front =atan2f(dataCmplxFront[(2*index_front)+1], dataCmplxRight[(2*index_front)]);
	arg_diffH = arg_left-arg_right;
	arg_diffV = arg_front - ((arg_left+arg_right)/2);
	arg_diffH= 360*(arg_diffH)/TWO_PI; //Conversion en degr�s
	arg_diffV= 360*(arg_diffV)/TWO_PI; //Conversion en degr�s


	//Mouvements d'alignement
	if((index_left == index_right) && (index_left == index_front)){
		//Si l'Epuck n'est pas align�, tourner selon la diff�rence de phase
		if(!align){
			SetVitesse(VITESSE_ALIGNEMENT);
			if(arg_diffH < -ARG_MAX_VIRAGE){
				TournerDroite();
			}else if(arg_diffH > ARG_MAX_VIRAGE){
				TournerGauche();
			}else{
				//L'Epuck est suffisament align� pour aller tout droit.
				SetVitesse(VITESSE_MAX);
				AllerToutDroit();
				align=TRUE;
			}
		}
		//Si l'Epuck est align�, aller tout droit jusqu'� avoir une grande diff�rence de phase
		else{
			if(arg_diffH < -ARG_MAX_TOUT_DROIT){
				align=FALSE;
			}else if(arg_diffH > ARG_MAX_TOUT_DROIT){
				align=FALSE;
			}
		}
	}
}

/*
*	Callback called when the demodulation of the four microphones is done.
*	We get 160 samples per mic every 10ms (16kHz)
*
*	params :
*	int16_t *data			Buffer containing 4 times 160 samples. the samples are sorted by micro
*							so we have [micRight1, micLeft1, micBack1, micFront1, micRight2, etc...]
*	uint16_t num_samples	Tells how many data we get in total (should always be 640)
*/

void ProcessAudio(int16_t *data, uint16_t num_samples){

	/*
	*
	*	We get 160 samples per mic every 10ms
	*	So we fill the samples buffers to reach
	*	1024 samples, then we compute the FFTs.
	*
	*/

	static uint16_t nb_samples = 0;
	static uint8_t mustSend = 0;

	//loop to fill the buffers
	for(uint16_t i = 0 ; i < num_samples ; i+=4){
		//construct an array of complex numbers. Put 0 to the imaginary part
		micRight_cmplx_input[nb_samples] = (float)data[i + MIC_RIGHT];
		micLeft_cmplx_input[nb_samples] = (float)data[i + MIC_LEFT];
		micBack_cmplx_input[nb_samples] = (float)data[i + MIC_BACK];
		micFront_cmplx_input[nb_samples] = (float)data[i + MIC_FRONT];

		nb_samples++;

		micRight_cmplx_input[nb_samples] = 0;
		micLeft_cmplx_input[nb_samples] = 0;
		micBack_cmplx_input[nb_samples] = 0;
		micFront_cmplx_input[nb_samples] = 0;

		nb_samples++;

		//stop when buffer is full
		if(nb_samples >= (2 * FFT_SIZE)){
			break;
		}
	}

	if(nb_samples >= (2 * FFT_SIZE)){
		/*	FFT proccessing
		*
		*	This FFT function stores the results in the input buffer given.
		*	This is an "In Place" function.
		*/

		doFFT_optimized(FFT_SIZE, micRight_cmplx_input);
		doFFT_optimized(FFT_SIZE, micLeft_cmplx_input);
		doFFT_optimized(FFT_SIZE, micFront_cmplx_input);
		doFFT_optimized(FFT_SIZE, micBack_cmplx_input);

		/*	Magnitude processing
		*
		*	Computes the magnitude of the complex numbers and
		*	stores them in a buffer of FFT_SIZE because it only contains
		*	real numbers.
		*
		*/
		arm_cmplx_mag_f32(micRight_cmplx_input, micRight_output, FFT_SIZE);
		arm_cmplx_mag_f32(micLeft_cmplx_input, micLeft_output, FFT_SIZE);
		arm_cmplx_mag_f32(micFront_cmplx_input, micFront_output, FFT_SIZE);
		arm_cmplx_mag_f32(micBack_cmplx_input, micBack_output, FFT_SIZE);

		//sends only one FFT result over 10 for 1 mic to not flood the computer
		//sends to UART3

		if(mustSend > 8){
			//signals to send the result to the computer
			chBSemSignal(&sendToComputer_sem);
			mustSend = 0;
		}

		nb_samples = 0;
		mustSend++;
		//Analyse de la fr�quence ayant la plus grande amplitude
		AnalyseFrequence(micBack_output);
		//Si l'audio n'est pas bloqu�
		if(!blockedAudio){
			//Si on est en mode retour, suivre la direction du son, sinon commander les d�placements en fonction de la fr�quence
			if(boolRecherche){
				if(frequence == FREQ_RECHERCHE){
					RechercheDirection(micLeft_output,micRight_output,micFront_output,
						micLeft_cmplx_input,micRight_cmplx_input,micFront_cmplx_input);
				}
			} else {
				CommandeDeplacement();
			}
		}
	}
}
//PUBLIC FUNCTIONS
void BloquerAudio(void){
	blockedAudio = TRUE;
}

void DebloquerAudio(void){
	blockedAudio = FALSE;
}


void StopperRecherche(void){
	boolRecherche = FALSE;
}

uint8_t TestRecherche(void){
	return boolRecherche;
}
