/*
 * 	obstacle.c
 *	Projet de Microinformatique: Guitar_Hero_Epuck2
 *  Created on: 4 mai 2020
 *  Author: Merlin et Pierre
 */

#include <stdio.h>
#include <stdlib.h>
#include <ch.h>
#include <hal.h>
#include <sensors/proximity.h>
#include "obstacle.h"
#include "audio.h"
#include "mouvement.h"
#include "main.h"

#define SEUIL_OBSTACLE 200
//Nombres de steps pour l'�vitement
#define STEPS_GRAND_VIRAGE 324
#define STEPS_PETIT_VIRAGE 162
#define STEPS_TOUT_DROIT 800
//index des capteurs IR
#define IR_FRONT_RIGHT 0
#define IR_FRONT_RIGHT_HALF 1
#define IR_FRONT_LEFT_HALF 6
#define IR_FRONT_LEFT 7
#define MAX_INDEX 8
//diff�rentes actions pour l'�vitement
typedef enum{
	TEST_OBSTACLE,
	VIRAGE,
	EVITEMENT,
	VIRAGE_INVERSE
}etat_evitement_t;
static etat_evitement_t etatEvitement;
//bool�ens
static uint8_t boolDirection = FALSE;
static uint8_t boolLongueur = FALSE;
static uint8_t boolObstacle = FALSE;
static uint8_t boolAttente = FALSE;
static uint8_t autonome = FALSE;

//Renvoie l'index du capteur qui d�tecte l'obstacle le plus proche.
uint8_t PlusProcheObstacle(void){
	int maxValue = SEUIL_OBSTACLE;
	uint8_t index = MAX_INDEX;
	for(uint8_t i = 0; i < MAX_INDEX; i++){
		int value = get_calibrated_prox(i);
		if(value<0){
			value= -value;
		}
		if(value > maxValue){
			maxValue = value ;
			index = i;
		}
	}
	return index;
}
//Renvoie un bool�en TRUE ou FALSE si l'un des capteurs d�passe le seuil.
uint8_t TestObstacle(void){
	for(uint8_t i = 0; i< MAX_INDEX; i++){
		int value = get_calibrated_prox(i);
		if(value<0){
			value = -value;
		}
		if(value > SEUIL_OBSTACLE){
			return TRUE;
		}
	}
	return FALSE;
}


//G�re l'�vitement des obstacles en parall�le de suivre le son (audio.c).
void DeplacementAutonome(void){
	switch(etatEvitement){
	//Test si il y a obstacle et lance le virage correspondant en fonction du capteur de proximit�.
	//Bloque �galement l'audio.
	case TEST_OBSTACLE :
		if(TestObstacle()){
			uint8_t index = PlusProcheObstacle();
			switch(index){
			//Diff�rents cas des capteurs
				case IR_FRONT_RIGHT :
					SetVitesse(VITESSE_MAX);
					BloquerAudio();
					TournerGauche();
					boolDirection = FALSE;
					boolLongueur = TRUE;
					etatEvitement = VIRAGE;
					break;
				case IR_FRONT_RIGHT_HALF :
					SetVitesse(VITESSE_MAX);
					BloquerAudio();
					TournerGauche();
					boolDirection = FALSE;
					boolLongueur = FALSE;
					etatEvitement = VIRAGE;
					break;
				case IR_FRONT_LEFT_HALF :
					SetVitesse(VITESSE_MAX);
					BloquerAudio();
					TournerDroite();
					boolDirection = TRUE;
					boolLongueur = FALSE;
					etatEvitement = VIRAGE;
					break;
				case IR_FRONT_LEFT :
					SetVitesse(VITESSE_MAX);
					BloquerAudio();
					TournerDroite();
					boolDirection = TRUE;
					boolLongueur = TRUE;
					etatEvitement = VIRAGE;
					break;
				default :
					break;
			}
		}
		break;
	//Test la fin du premier virage en fonction de si c'est un grand ou un petit.
	//Va tout droit si le premier virage est termin�
	case VIRAGE :
		if(boolLongueur){
			if(TestFinSteps(STEPS_GRAND_VIRAGE)){
				AllerToutDroit();
				etatEvitement = EVITEMENT;
			}
		}else{
			if(TestFinSteps(STEPS_PETIT_VIRAGE)){
				AllerToutDroit();
				etatEvitement = EVITEMENT;
			}
		}
		break;
	//Test la fin d'ALLER_TOUT_DROIT et lance le virage inverse, pour rester dans l'alignement initial
	case EVITEMENT:
		if(TestFinSteps(STEPS_TOUT_DROIT)){
			if(boolDirection){
				TournerGauche();
				etatEvitement = VIRAGE_INVERSE;
			}else{
				TournerDroite();
				etatEvitement = VIRAGE_INVERSE;
			}
		}
		break;
	//Test la fin du VIRAGE_INVERSE, revient � TEST_OBSTACLE et relance l'audio
	case VIRAGE_INVERSE:
		if(boolLongueur){
			if(TestFinSteps(STEPS_GRAND_VIRAGE)){
				AllerToutDroit();
				etatEvitement = TEST_OBSTACLE;
				DebloquerAudio();
			}
		}else{
			if(TestFinSteps(STEPS_PETIT_VIRAGE)){
				AllerToutDroit();
				etatEvitement = TEST_OBSTACLE;
				DebloquerAudio();
			}
		}
		break;
	}
}




//Thread Obstacle: Lance d�placement autonome si autonome, sinon change boolObstacle selon si il y a un obstacle ou non.
 static THD_WORKING_AREA(waObstacleThd, 128);
 static THD_FUNCTION(ObstacleThd, arg) {

 	chRegSetThreadName(__FUNCTION__);
 	(void)arg;

 	while(1){
 		if(autonome){
 			DeplacementAutonome();
 		}else{
 			//Si attente: lancement du temps d'invincibilit�
 			if(boolAttente){
 				boolAttente = FALSE;
 				chThdSleepMilliseconds(TEMPS_INVINCIBLE);
 			} else {
				if(TestObstacle()){
 					boolObstacle = TRUE;
				}
				else{
					boolObstacle=FALSE;
				}
 			}
 		}
 	}
 }
 //PUBLIC FUNCTIONS

 void ObstacleInit(){
  	chThdCreateStatic(waObstacleThd, sizeof(waObstacleThd), NORMALPRIO, ObstacleThd, NULL);
  }

 uint8_t GetObstacle(void){
 	return boolObstacle;
 }

 void ResetObstacle(void){
 	boolObstacle = FALSE;
 	boolAttente = TRUE;
 }

 void LancerAutonome(void){
 	autonome = TRUE;
 	etatEvitement=TEST_OBSTACLE;
 }

 void StopperAutonome(void){
 	DebloquerAudio();
 	StopperMoteurs();
 	etatEvitement = TEST_OBSTACLE;
 	autonome = FALSE;
 }



