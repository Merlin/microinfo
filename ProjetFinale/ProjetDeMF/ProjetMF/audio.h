/*
 * 	audio.h
 *	Projet de Microinformatique: Guitar_Hero_Epuck2
 *  Created on: 4 mai 2020
 *  Author: Merlin et Pierre
 */

#ifndef AUDIO_H_
#define AUDIO_H_

#include <ch.h>
#include <hal.h>
//Fonction appel�e � chaque nouvelle donn�e des microphones, appelle toutes les fonctions de traitement du son
void ProcessAudio(int16_t *data, uint16_t num_samples);
//Bloque certaines fonctions de traitement du son
void BloquerAudio(void);
//D�bloque ces m�mes fonctions
void DebloquerAudio(void);
//Verifie si la note de retour a �t� jou�e. Retourne l'information au main.
uint8_t TestRecherche(void);
//Stop le mode retour.
void StopperRecherche(void);

#endif
