/*
 * 	main.c
 *	Projet de Microinformatique: Guitar_Hero_Epuck2
 *  Created on: 4 mai 2020
 *  Author: Merlin et Pierre
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <leds.h>
#include <audio/audio_thread.h>
#include <usbcfg.h>
#include <main.h>
#include <chprintf.h>
#include <motors.h>
#include <audio/microphone.h>
#include <fft.h>
#include <audio/play_melody.h>
#include <selector.h>
#include <sensors/proximity.h>
#include "obstacle.h"
#include "ch.h"
#include "hal.h"
#include "memory_protection.h"
#include "mouvement.h"
#include "audio.h"

#define DISTANCE_WIN 				100 		//Distance de 1 m�tre � parcourir pour gagner
#define VIES_DEPART 				6			//Nombre de vies au d�part
#define TEMPS_ATTENTE_INIT_MELODIE  100			//Temps d'attente apr�s l'initialisation de play_melody

static uint8_t vies =0;
static uint8_t boolJeu=FALSE;							//D�termine le mode jeu ou le mode retour
static int previousSelector=0;							//M�morise l'ancienne valeur du selector

//d�claration du bus de message
messagebus_t bus;
MUTEX_DECL(bus_lock);
CONDVAR_DECL(bus_condvar);

//d�finition de la m�lodie du choc personnalis�e
static const uint16_t loseOneLife_melody[] = {
	NOTE_C6, NOTE_A5,NOTE_C6, NOTE_B5, NOTE_G5,
};

static const float loseOneLife_tempo[] = {
	10, 10, 15, 10, 20,
};

static const melody_t melodies[1] = {
	{
	.notes = loseOneLife_melody,
	.tempo = loseOneLife_tempo,
	.length = sizeof(loseOneLife_melody)/sizeof(uint16_t),
	},
};

//fonctions utilisant les LEDs
void AllumerLeds(void){
	uint8_t yes = TRUE;

	palWritePad(GPIOD, GPIOD_LED1, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED3, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED5, yes ? 0 : 1);
	palWritePad(GPIOD, GPIOD_LED7, yes ? 0 : 1);
}

void EteindreLeds(void){
	clear_leds();
}


//Reset les conditions de d�part du jeu, allume les leds et lance une musique (pour un spectacle son et lumi�re)
void LancerJeu(void){
	//stop ce que fait l'Epuck
	BloquerMoteurs();
	StopperRecherche();
	StopperAutonome();
	ResetObstacle();
	//Lance la musique et les LEDs
	playMelody(MARIO_START,ML_SIMPLE_PLAY,NULL);
	AllumerLeds();
	chThdSleepMilliseconds(TEMPS_INVINCIBLE);
	EteindreLeds();
	//Initialise les conditions de d�part
	vies=VIES_DEPART;
	boolJeu = TRUE;
	InitMouvement();
 	previousSelector = get_selector();
 	DebloquerMoteurs();
}


int main(void)
{
	//initialisation
    halInit();
    chSysInit();
    mpu_init();
    motors_init();
    messagebus_init(&bus, &bus_lock, &bus_condvar);
    proximity_start();
    calibrate_ir();
    dac_start();
    playMelodyStart();
    chThdSleepMilliseconds(TEMPS_ATTENTE_INIT_MELODIE);
    ObstacleInit();
    mic_start(&ProcessAudio);

    LancerJeu();

    //d�finition du bus pour proximity
    messagebus_topic_t *proximity_topic = messagebus_find_topic_blocking(&bus, "/proximity");

    proximity_msg_t proximity_values;

    while (TRUE) {
    	//attendre les nouvelles valeurs de proximity
    	messagebus_topic_wait(proximity_topic, &proximity_values, sizeof(proximity_values));
    	//si le selector a chang�, r�initialiser
    	if(get_selector() != previousSelector){
    		LancerJeu();
    	}
    	//Si il y a la note de retour, lancer le mode retour
    	if(TestRecherche() && boolJeu){
    		LancerAutonome();
    		boolJeu=FALSE;
    		AllumerLeds();
    	}
    	//Si nous sommes en jeu
    	if(boolJeu){
    		//tester la condition de victoire et lance la musique le cas �ch�ant
    		double position = GetPosition();
    		if((position>=DISTANCE_WIN)&&(vies>0)){
    			stopCurrentMelody();
				waitMelodyHasFinished();
				playMelody(MARIO_FLAG,ML_SIMPLE_PLAY, NULL);
				AllumerLeds();
				vies=0;
    		}
    		//Test si il y a un obstacle et d�cr�mente une vie le cas �ch�ant
    		if(GetObstacle() && vies > 0){
				vies--;
				ResetObstacle();
				//si le joueur n'a plus de vie, il a perdu
				if(vies <= 0){
					stopCurrentMelody();
					waitMelodyHasFinished();
					playMelody(MARIO_DEATH,ML_SIMPLE_PLAY, NULL);
				} else {
					AllumerLeds();
					playMelody(EXTERNAL_SONG,ML_SIMPLE_PLAY, &melodies[0]);
				}
			    chThdSleepMilliseconds(TEMPS_INVINCIBLE);
			    EteindreLeds();
			}
    	}
    }
}


#define STACK_CHK_GUARD 0xe2dee396
uintptr_t __stack_chk_guard = STACK_CHK_GUARD;

void __stack_chk_fail(void)
{
    chSysHalt("Stack smashing detected");
}
