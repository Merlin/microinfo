/*
 * 	mouvement.h
 *	Projet de Microinformatique: Guitar_Hero_Epuck2
 *  Created on: 4 mai 2020
 *  Author: Merlin et Pierre
 */

#ifndef MOUVEMENT_H_
#define MOUVEMENT_H_
//Diff�rentes vitesses des moteurs
#define VITESSE_MAX 	   600
#define VITESSE_VIRAGE     400
#define VITESSE_ALIGNEMENT 200
//Change la vitesse des moteurs
void SetVitesse(int nouvelleVitesse);
//Fait avancer l'Epuck tout droit ind�finiment et lance Update() si besoin
void AllerToutDroit(void);
//Fait reculer l'Epuck et lance Update() si besoin
void AllerEnArriere(void);
//Fait tourner l'Epuck � gauche et lance Update() si besoin
void TournerGauche(void);
//Fait tourner l'Epuck � droite et lance Update() si besoin
void TournerDroite(void);
//Renvoie la position de l'Epuck sur l'axe vertical initial
double GetPosition(void);
//Initialise la position et l'angle de l'Epuck � 0
void InitMouvement(void);
//Arrete l'Epuck et lance Update() si besoin
void StopperMoteurs(void);
//Bloque toutes les fonctions qui d�placent l'Epuck
void BloquerMoteurs(void);
//D�bloque toutes les fonctions qui d�placent l'Epuck
void DebloquerMoteurs(void);
//V�rifie si le compteur des moteurs a atteint le nombre de steps donn�s en argument
uint8_t TestFinSteps(int32_t limiteSteps);

#endif /* MOUVEMENT_H_ */
