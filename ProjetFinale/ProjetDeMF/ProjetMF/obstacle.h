/*
 * 	obstacle.h
 *	Projet de Microinformatique: Guitar_Hero_Epuck2
 *  Created on: 4 mai 2020
 *  Author: Merlin et Pierre
 */

#ifndef OBSTACLE_H_
#define OBSTACLE_H_
#include <ch.h>
#include <hal.h>

//initialise le thread Obstacle
void ObstacleInit(void);
//renvoie si il y a eu un obstacle sous la forme d'un bool�en
uint8_t GetObstacle(void);
//lance le mode attente du thread Obstacle
void ResetObstacle(void);
//lance le mode autonome
void LancerAutonome(void);
//stop le mode autonome
void StopperAutonome(void);

#endif /* OBSTACLE_H_ */
