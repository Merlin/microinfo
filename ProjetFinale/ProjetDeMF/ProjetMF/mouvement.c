/*
 * 	mouvement.c
 *	Projet de Microinformatique: Guitar_Hero_Epuck2
 *  Created on: 4 mai 2020
 *  Author: Merlin et Pierre
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <motors.h>
#include "main.h"
#include "mouvement.h"
#include "ch.h"
#include "hal.h"
//Conversions compteurs vers systeme international
#define COUNT_TO_DEGRES 		0.2777
#define COUNT_TO_CM 			0.013
#define DEGRES_TO_RADIANS		0.0174
//Angle max en degr�s
#define ANGLE_MAX 				360
//Position et angle de l'Epuck par rapport � son axe vertical initial
static double position=0;
static double angle=0;
//Vitesse des moteurs
static int vitesse = VITESSE_MAX;
//Bool�en de bloquage des mouvements
static uint8_t blocked=0;
//Diff�rents mouvements de l'Epuck
typedef enum{
	TOUT_DROIT = 0,
	EN_ARRIERE,
	VIRAGE_GAUCHE,
	VIRAGE_DROITE,
	STOP
}etat_mouvement_t;

static etat_mouvement_t etatMouvement;


//Remet les compteurs de moteurs � 0
void ResetCompteur(void){
	left_motor_set_pos(0);
	right_motor_set_pos(0);
}
//Calcul l'angle et la position de l'Epuck en fonction du mouvement qu'il vient de terminer
void Update(void){
	int32_t compteur = right_motor_get_pos();
	if(compteur < 0){
		compteur = -compteur;
	}
//Calcul si l'Epuck a avanc� sur son axe vertical (projection)
	double angleRad = angle * DEGRES_TO_RADIANS;
	double cosAngle = cos(angleRad);
	double hyp = compteur * COUNT_TO_CM;
	double delta = hyp * cosAngle;
//Calcul si l'Epuck a tourn� sur lui-m�me
	double deltaAngle = compteur * COUNT_TO_DEGRES;
//Ajout des changements en fonction du mouvement
	switch(etatMouvement){
		case TOUT_DROIT:
			position += delta;
			break;
		case EN_ARRIERE:
			position -= delta;
			break;
		case VIRAGE_GAUCHE:
			angle += deltaAngle;
			break;
		case VIRAGE_DROITE:
			angle -= deltaAngle;
			break;
		case STOP:
			break;
	}
//Retourner l'angle dans la plage [0;360]
	while(angle > ANGLE_MAX){
		angle -= ANGLE_MAX;
	}
	if(angle < 0){
		angle = ANGLE_MAX - angle;
	}
//Remettre les compteurs � 0 pour le prochain mouvement
	ResetCompteur();
}

//PUBLIC FUNCTION

void StopperMoteurs(void){
	if(etatMouvement != STOP){
		Update();
		etatMouvement = STOP;
		left_motor_set_speed(0);
		right_motor_set_speed(0);
	}
}

void SetVitesse(int nouvelleVitesse){
	if(nouvelleVitesse>=VITESSE_MAX){
		vitesse= VITESSE_MAX;
	}
	else{
		vitesse= nouvelleVitesse;
	}
}



void InitMouvement(void){
	position = 0;
	angle = 0;
	StopperMoteurs();
}


void AllerToutDroit(void){
	if(!blocked){
		if(etatMouvement != TOUT_DROIT){
			Update();
			etatMouvement = TOUT_DROIT;
			left_motor_set_speed(vitesse);
			right_motor_set_speed(vitesse);
		}
	}
}

void AllerEnArriere(void){
	if(!blocked){
		if(etatMouvement != EN_ARRIERE){
			Update();
			etatMouvement = EN_ARRIERE;
			left_motor_set_speed(-vitesse);
			right_motor_set_speed(-vitesse);
		}
	}
}

void TournerGauche(void){
	if(!blocked){
		if(etatMouvement != VIRAGE_GAUCHE){
			Update();
			etatMouvement = VIRAGE_GAUCHE;
			left_motor_set_speed(-vitesse);
			right_motor_set_speed(vitesse);
		}
	}
}

void TournerDroite(void){
	if(!blocked){
		if(etatMouvement != VIRAGE_DROITE){
			Update();
			etatMouvement = VIRAGE_DROITE;
			left_motor_set_speed(vitesse);
			right_motor_set_speed(-vitesse);
		}
	}
}



double GetPosition(void){
	Update();
	return position;
}



void BloquerMoteurs(void){
	StopperMoteurs();
	blocked = TRUE;
}

void DebloquerMoteurs(void){
	blocked = FALSE;
}


uint8_t TestFinSteps(int32_t limiteSteps){
	int32_t compteur;
	compteur = left_motor_get_pos();
	if(compteur<0){
		compteur = -compteur;
	}
	if(compteur >= limiteSteps){
		return TRUE;
		ResetCompteur();
	}else{
		return FALSE;
	}
}


